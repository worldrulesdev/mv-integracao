function beforeTaskSave(colleagueId, nextSequenceId, userList) {
	log.info("Entrou no beforeTaskSave");

	//Função para modularizar a query
	var column = function (name) {
		return "\'" + name + "\'"
	}

	// Variaveis 
	var fantasia = hAPI.getCardValue("fantasia")
	var fornecedor = hAPI.getCardValue("fornecedor")
	var tipo = hAPI.getCardValue("tipo")
	var tipoConstituicao = hAPI.getCardValue("tipoConstituicao")
	var nacionalidade = hAPI.getCardValue("nacionalidade")
	var cpf = hAPI.getCardValue("cpf")
	var cnpj = hAPI.getCardValue("cnpj")
	var contribuinte = hAPI.getCardValue("contribuinte")
	var inscricaoest = hAPI.getCardValue("inscricaoest")
	var inscricaomun = hAPI.getCardValue("inscricaomun")
	var inscricaoinss = hAPI.getCardValue("inscricaoinss")
	var pis = hAPI.getCardValue("pis")
	var quantidade = hAPI.getCardValue("quantidade")
	var pais = hAPI.getCardValue("pais")
	var nomepais = hAPI.getCardValue("nomepais")
	var cep = hAPI.getCardValue("cep")
	var rua = hAPI.getCardValue("rua")
	var numero = hAPI.getCardValue("numero")
	var bairro = hAPI.getCardValue("bairro")
	var cidade = hAPI.getCardValue("cidade")
	var uf = hAPI.getCardValue("uf")
	var complemento = hAPI.getCardValue("complemento")
	var codCidade = hAPI.getCardValue("codCidade")
	var estado = hAPI.getCardValue("estado")
	var cbo = hAPI.getCardValue("cbo")
	var setor = hAPI.getCardValue("setor")
	var setorvazio = hAPI.getCardValue("setorvazio")
	var ativo = hAPI.getCardValue("ativo")
	var pagamento = hAPI.getCardValue("pagamento")
	var formula = hAPI.getCardValue("formula")
	var ranking = hAPI.getCardValue("ranking")
	var avaliacao = hAPI.getCardValue("avaliacao")
	var simplesnacional = hAPI.getCardValue("simplesnacional")
	var codigoafe = hAPI.getCardValue("codigoafe")
	var banco = hAPI.getCardValue("banco")
	var descAgencia = hAPI.getCardValue("descAgencia")
	var conta = hAPI.getCardValue("conta")
	var ccm = hAPI.getCardValue("ccm")
	var statusfornecedor = hAPI.getCardValue("statusfornecedor")



	// Query para inserir na tabela TEMP_FORNECEDOR
	var query = "INSERT INTO TEMP_FORNECEDOR (cd_fornecedor, nm_fornecedor, nm_fantasia, tp_fornecedor, ds_endereco, nr_cgc_cpf, cd_cidade, sn_ativo, tp_cliente_forn ) VALUES (seq_fornecedor.nextval," + column(fornecedor) + ", " + column(fantasia) + ", " + column(tipo) + ", "+ column(rua) + ", '02792238038', 3042, 'S', 'P' )";




	// Teste do resultado da query
	log.info("Resultado da query: " + query);

	//Padrão da conexão Oracle
	var ic = new javax.naming.InitialContext();
	var ds = ic.lookup("jdbc/MVsml");
	var conn = ds.getConnection();
	var stmt = conn.createStatement();

	//Executando a query
	stmt.executeQuery(query);

	stmt.close();
	conn.close();



}