$(document).ready(function () {
    $('#formulario').validate({
        rules: {
            codigo: {
                required: true,
                minlength: 3
            },
            fornecedor: {
                required: true,
                minlength: 5
            },
            fantasia: {
                required: true,
                minlength: 5
            },
            tipo: {
                required: true
            },
            tipoConstituicao: {
                required: true,
                minlength: 5
            },
            contribuinte: {
                required: true,
                minlength: 5
            },
            cpf: {
                required: true,
                minlength: 12
            },
            inscricaoest: {
                required: true,
                minlength: 5
            },
            inscricaomun: {
                required: true,
                minlength: 5
            },
            inscricaoinss: {
                required: true,
                minlength: 5
            },
            pis: {
                required: true,
                minlength: 14
            },
            quantidade: {
                required: true,
                minlength: 1
            },
            pais: {
                required: true,
                minlength: 1
            },
            nomepais: {
                required: true,
                minlength: 3
            },
            cep: {
                required: true,
                minlength: 8
            },
            setor: {
                required: true,
                minlength: 3
            },
            setorvazio:{
                required: true, 
                minlength: 3
            },
            codigoafe:{
                required: true, 
                minlength: 3
            },
            banco:{
                required: true, 
                minlength: 3
            },
            descAgencia:{
                required: true,
                minlength: 5
            },
            conta:{
                required: true,
                minlength: 5
            },
            ccm:{
                required: true,
                minlength: 2
            }
        },
        messages: {
            codigo: {
                required: "O campo código é obrigatório.",
                minlength: "O campo código deve conter no mínimo 2 números."

            },
            fornecedor: {
                required: "O campo fornecedor é obrigatório.",
                minlength: "O campo fornecedor deve conter no mínimo 5 caracteres."
            },
            fantasia: {
                required: "O campo fantasia é obrigatório.",
                minlength: "O campo fantasia deve conter no mínimo 5 caracteres."
            },
            tipo: {
                required: "Selecione um tipo."
            },
            tipoConstituicao: {
                required: "O campo tipo da constituíção é obrigatório.",
                minlength: "O campo tipo da constituíção deve conter no mínimo 5 caracteres."
            },
            contribuinte: {
                required: "O campo fornecedor é obrigatório.",
                minlength: "O campo código deve conter no mínimo 5 números."
            },
            cpf: {
                required: "O campo cpf é obrigatório.",
                minlength: "O campo cpf deve conter os 11 caracteres."
            },
            inscricaoest: {
                required: "O campo inscricao estadual é obrigatório.",
                minlength: "O campo inscricao estadual deve conter os 5 caracteres."
            },
            inscricaomun: {
                required: "O campo inscricao municipal é obrigatório.",
                minlength: "O campo inscricao municipal deve conter os 5 caracteres."
            },
            inscricaoinss: {
                required: "O campo inscricao INSS é obrigatório.",
                minlength: "O campo inscricao INSS deve conter os 5 caracteres."
            },
            pis: {
                required: "O campo inscricao PIS é obrigatório.",
                minlength: "O campo inscricao PIS deve conter os 11 caracteres."
            },
            quantidade: {
                required: "O campo quantidade de departamentos é obrigatório.",
                minlength: "O campo quantidade de departamentos deve conter os 1 caracter."
            },
            pais: {
                required: "O campo nº do país é obrigatório.",
                minlength: "O campo nº do país deve conter os 1 número."
            },
            nomepais: {
                required: "O campo nome do país é obrigatório.",
                minlength: "O campo nome do país deve conter os 3 caracteres."
            },
            cep: {
                required: "O campo cep é obrigatório.",
                minlength: "O campo cep deve conter os 8 números."
            },
            setor: {
                required: "O campo setor é obrigatório.",
                minlength: "O campo setor deve conter os 3 caracteres."
            },
            setorvazio: {
                required: "O campo setor é obrigatório.",
                minlength: "O campo setor deve conter os 3 caracteres."
            },
            codigoafe: {
                required: "O campo código AFE é obrigatório.",
                minlength: "O campo código AFE deve conter os 3 caracteres."
            },
            banco:{
                required: "O campo banco é obrigatório.", 
                minlength: "O campo banco deve conter pelo menos 3 caracteres."
            },
            descAgencia:{
                required: "O campo descrição da agência é obrigatório.",
                minlength: "O campo descrição da agência deve conter os 5 caracteres."
            },
            conta:{
                required: "O campo conta é obrigatório.",
                minlength: "O campo conta deve conter os 5 números."
            },
            ccm:{
                required: "O campo CCM é obrigatório.",
                minlength: "O campo CCM deve conter os 2 números."
            }

        }
    });
});